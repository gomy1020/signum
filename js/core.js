var headerOpen = false;
var pagerOpen = false;
var firstRun = true;
var resizeTimeout;

$(document).ready(function(){
    /*$('#header').mouseenter(openHeader);
    $('#header').mouseleave(closeHeader);
    $('#header').mouseup(function(){
        if(headerOpen){
            closeHeader();
        } else {
            openHeader();
        }
    });*/

    $('.nav-toggle').click(function(){
        if (headerOpen){
            closeHeader();
        } else {
            openHeader();
        }
    });

    $('.close-popup').click(function(){
        $(this).parent().animate({
            'bottom': '-34%'
        },300)
    })

    $('strong.open-popup').click(function(){
        $('.popup').animate({
            'bottom': '0'
        },300)
    })

    $('.slider-nav').mouseenter(openPager);
    $('.slider-nav').mouseleave(closePager);
    $('.slider-arrow').click(function(){
        if(pagerOpen){
            closePager();
        } else {
            openPager();
        }
    });
    
    $('#slider .background .slide').each(function(){
        $('.slider-shim').append('<div class="slide-shim"></div>');

        var img = $(this).find('img');
        $(this).backstretch(img.prop('src'));
        img.hide();
    });

    /*$('#slider .background .slide').imagefill({
        resizeOnly: true
    });*/


    $('.slider-shim').cycle({
        fx: 'scrollHorz',
        speed: 1000,
        timeout: 13000,
        easing: 'easeInOutExpo',
        // prev: $('.slider-prev'),
        // next: $('.slider-next'),
        pager: $('.slider-pager'),
        before: doSlide
    });

});

$(window).load(function(){
    $(window).resize(doResize);
    doResize(true);
    // updateBlur();
});

function openHeader(){
    if(!headerOpen){
        $('#header').stop().animate({left: 160}, 500, 'easeInOutExpo');
        $('.nav-toggle').css('background','url("img/close-menu.jpg")')
        headerOpen = true;
    }
}

function closeHeader(){
    if(headerOpen){
        $('#header').stop().animate({left: 0}, 500, 'easeInOutExpo');
        $('.nav-toggle').css('background','url("img/open-menu.jpg")')
        headerOpen = false;
    }
}

function openPager(){
    if(!pagerOpen){
        $('.slider-nav-inner').stop().animate({left: -($('.slider-pager').outerWidth()+81)}, 500, 'easeInOutExpo');
        pagerOpen = true;
    }
}

function closePager(){
    if(pagerOpen){
        $('.slider-nav-inner').stop().animate({left: -81}, 500, 'easeInOutExpo');
        pagerOpen = false;
    }
}

function doSlide(curr, next, opts, fwd){
    if(firstRun){
        $('#slider .background .slide').hide();
        $('#slider .background .slide').eq(0).show();

        $('#slider .foreground .slide').hide();
        $('#slider .foreground .slide').eq(0).show();
        firstRun = false;

        return;
    }

    var bgCurr = $('#slider .background .slide').eq(opts.currSlide).find('.backstretch img');
    var bgNext = $('#slider .background .slide').eq(opts.nextSlide).find('.backstretch img');

    var blurCurr = $('#slider .foreground .slide').eq(opts.currSlide).find('.slide-pane-blurmask img');
    var blurNext = $('#slider .foreground .slide').eq(opts.nextSlide).find('.slide-pane-blurmask img');

    var paneCurr = $('#slider .foreground .slide').eq(opts.currSlide).find('.slide-pane');
    var paneNext = $('#slider .foreground .slide').eq(opts.nextSlide).find('.slide-pane');

    var offsetCurr = -$(window).width()/2+485-parseFloat(paneCurr.css('margin-left'))+parseFloat(bgCurr.css('left'));
    var offsetNext = -$(window).width()/2+485-parseFloat(paneNext.css('margin-left'))+parseFloat(bgNext.css('left'));

    if(fwd){
        $('#slider .background .slide').eq(opts.currSlide).stop().css({top: 0, left: 0}).animate({left: -$(window).width()}, opts.speed, opts.easing, function(){
            $(this).hide();
        });
        $('#slider .background .slide').eq(opts.nextSlide).stop().css({top: 0, left: $(window).width()}).show().animate({left: 0}, opts.speed, opts.easing);

        $('#slider .foreground .slide').eq(opts.currSlide).stop().css({top: 0, left: '50%'}).animate({left: $(window).width()+485}, opts.speed, opts.easing, function(){
            $(this).hide();
        });
        $('#slider .foreground .slide').eq(opts.nextSlide).stop().css({top: 0, left: -485}).show().animate({left: '50%'}, opts.speed, opts.easing);

        blurCurr.stop().css({left: offsetCurr}).animate({left: -$(window).width() + offsetCurr}, opts.speed, opts.easing);
        blurNext.stop().css({left: $(window).width() + offsetNext}).show().animate({left: offsetNext}, opts.speed, opts.easing);
    } else {
        $('#slider .background .slide').eq(opts.currSlide).stop().css({top: 0, left: 0}).animate({left: $(window).width()}, opts.speed, opts.easing, function(){
            $(this).hide();
        });
        $('#slider .background .slide').eq(opts.nextSlide).stop().css({top: 0, left: -$(window).width()}).show().animate({left: 0}, opts.speed, opts.easing);

        $('#slider .foreground .slide').eq(opts.currSlide).stop().css({top: 0, left: '50%'}).animate({left: -485}, opts.speed, opts.easing, function(){
            $(this).hide();
        });
        $('#slider .foreground .slide').eq(opts.nextSlide).stop().css({top: 0, left: $(window).width()+485}).show().animate({left: '50%'}, opts.speed, opts.easing);

        blurCurr.stop().css({left: offsetCurr}).animate({left: $(window).width() + offsetCurr}, opts.speed, opts.easing);
        blurNext.stop().css({left: -$(window).width() + offsetNext}).show().animate({left: offsetNext}, opts.speed, opts.easing);
    }

    // updateBlur();
}

function updateBlur(){
    $('#slider .foreground .slide').each(function(index){
        var bg = $('#slider .background .slide').eq(index).find('.backstretch img');
        var blur = $(this).find('.slide-pane-blurmask img');
        var pane = $(this).find('.slide-pane');

        blur.width(bg.width()).height(bg.height()).css({top: bg.css('top'), left: (-$(window).width()/2+485-parseFloat(pane.css('margin-left'))+parseFloat(bg.css('left')))+'px'});
    });
}

function doResize(skipTimeout){
    // $('#slider').height($(window).height() - 135);
    updateBlur();

    if(resizeTimeout) clearTimeout(resizeTimeout);

    if(skipTimeout != true){
        resizeTimeout = setTimeout(finishResizing, 100);
    }
}

function finishResizing(){
    // $('.backstretch').height($('#slider').height());
    
}
