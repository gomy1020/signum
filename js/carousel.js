var pagerOpen = false;
var firstRun = true;

$(document).ready(function(){

    // $('#slider').css('opacity', 0);
    $('#slider .slide').hide();
    $('#slider .background .slide').first().show();
    $('#slider .foreground .slide').first().show();

    $('.slider-nav').hide();
    $('.slider-nav').mouseenter(openPager);
    $('.slider-nav').mouseleave(closePager);
    $('body').on('click', '.slider-arrow', function(e){
        e.preventDefault();
        if(pagerOpen){
            closePager();
        } else {
            openPager();
        }
    });
    
    $('#slider .background .slide').each(function(){
        $('.slider-shim').append('<div class="slide-shim"></div>');
    });

    $('#slider .background .slide').imagefill({
        runOnce: true
    });

    // var carouselTimeout = setTimeout(startCarousel, 5000);

    $('#slider').imagesLoaded().done(function(img){
        // clearTimeout(carouselTimeout);
        // console.log('images loaded');
        // showFirstSlide();
        startCarousel();
    });

    $(window).resize(doResize);
});

/*$(window).load(function(){
    doResize(true);

    // $('.slide img').show();

    console.log('window load');
    // startCarousel();
});*/

function startCarousel(){
    $('.slide').show();
    $('.slider-nav').show();
    // $('#slider').stop().delay(500).animate({opacity: 1}, 500);

    doResize();
    $('.slider-shim').cycle({
        fx: 'scrollHorz',
        speed: 1500,
        timeout: 6000,
        easing: 'easeInOutExpo',
        pager: $('.slider-pager'),
        before: doSlide
    });
}

function openPager(){
    if(!pagerOpen){
        $('.slider-nav-inner').stop().animate({left: -($('.slider-pager').outerWidth()+81)}, 500, 'easeInOutExpo');
        pagerOpen = true;
    }
}

function closePager(){
    if(pagerOpen){
        $('.slider-nav-inner').stop().animate({left: -81}, 500, 'easeInOutExpo');
        pagerOpen = false;
    }
}

function showFirstSlide(){
    $('#slider .background .slide').hide();
    $('#slider .background .slide').eq(0).show();

    $('#slider .foreground .slide').hide();
    $('#slider .foreground .slide').eq(0).show();
}

function doSlide(curr, next, opts, fwd){
    if(firstRun){
        showFirstSlide();
        firstRun = false;

        return;
    }

    var bgCurr = $('#slider .background .slide').eq(opts.currSlide).find('img');
    var bgNext = $('#slider .background .slide').eq(opts.nextSlide).find('img');

    var blurCurr = $('#slider .foreground .slide').eq(opts.currSlide).find('.slide-pane-blurmask img');
    var blurNext = $('#slider .foreground .slide').eq(opts.nextSlide).find('.slide-pane-blurmask img');

    var paneCurr = $('#slider .foreground .slide').eq(opts.currSlide).find('.slide-pane');
    var paneNext = $('#slider .foreground .slide').eq(opts.nextSlide).find('.slide-pane');

    var offsetCurr = -$(window).width()/2+485-parseFloat(paneCurr.css('margin-left'))+parseFloat(bgCurr.css('left'));
    var offsetNext = -$(window).width()/2+485-parseFloat(paneNext.css('margin-left'))+parseFloat(bgNext.css('left'));

    var offsetFG = 50;

    if(fwd){
        $('#slider .background .slide').eq(opts.currSlide).stop().css({top: 0, left: 0}).animate({left: -$(window).width()}, opts.speed, opts.easing, function(){
            $(this).hide();
            // doResize();
        });
        $('#slider .background .slide').eq(opts.nextSlide).stop().css({top: 0, left: $(window).width()}).show().animate({left: 0}, opts.speed, opts.easing);

        $('#slider .foreground .slide').eq(opts.currSlide).stop().css({top: 0, left: '50%'}).animate({left: $(window).width()+485+offsetFG}, opts.speed, opts.easing, function(){
            $(this).hide();
        });
        $('#slider .foreground .slide').eq(opts.nextSlide).stop().css({top: 0, left: -485-offsetFG}).show().animate({left: '50%'}, opts.speed, opts.easing);

        blurCurr.stop().css({left: offsetCurr}).animate({left: -$(window).width() + offsetCurr}, opts.speed, opts.easing);
        blurNext.stop().css({left: $(window).width() + offsetNext}).show().animate({left: offsetNext}, opts.speed, opts.easing);
    } else {
        $('#slider .background .slide').eq(opts.currSlide).stop().css({top: 0, left: 0}).animate({left: $(window).width()}, opts.speed, opts.easing, function(){
            $(this).hide();
            // doResize();
        });
        $('#slider .background .slide').eq(opts.nextSlide).stop().css({top: 0, left: -$(window).width()}).show().animate({left: 0}, opts.speed, opts.easing);

        $('#slider .foreground .slide').eq(opts.currSlide).stop().css({top: 0, left: '50%'}).animate({left: -485-offsetFG}, opts.speed, opts.easing, function(){
            $(this).hide();
        });
        $('#slider .foreground .slide').eq(opts.nextSlide).stop().css({top: 0, left: $(window).width()+485+offsetFG}).show().animate({left: '50%'}, opts.speed, opts.easing);

        blurCurr.stop().css({left: offsetCurr}).animate({left: $(window).width() + offsetCurr}, opts.speed, opts.easing);
        blurNext.stop().css({left: -$(window).width() + offsetNext}).show().animate({left: offsetNext}, opts.speed, opts.easing);
    }

    doResize();
}

function updateBlur(){
    $('#slider .foreground .slide').each(function(index){
        var bg = $('#slider .background .slide').eq(index).find('img');
        var blur = $(this).find('.slide-pane-blurmask img');
        var pane = $(this).find('.slide-pane');

        blur.width(bg.width()).height(bg.height()).css({top: bg.css('top'), left: (-$(window).width()/2+485-parseFloat(pane.css('margin-left'))+parseFloat(bg.css('left')))+'px'});
    });
}

function doResize(skipTimeout){
    fitImages('#slider .background .slide');
    updateBlur();
}

function fitImages(selector) {
    var $container = $(selector);
    $container.each(function() {
        var img = $(this).find('img');
        var imageAspect = img.width() / img.height();
        var containerW = $(this).outerWidth(),
            containerH = $(this).outerHeight();

        var containerAspect = containerW/containerH;
        if (containerAspect < imageAspect) {
          // taller
          img.css({
              width: img.width() * containerH / img.height(),
              height: containerH,
              top:0,
              left:-(containerH*imageAspect-containerW)/2
            });
        } else {
          // wider
          img.css({
              width: containerW,
              height: img.height() * containerW / img.width(),
              top:-(containerW/imageAspect-containerH)/2,
              left:0
            });
        }
  });
}