var headerOpen = false;

$(document).ready(function(){

    $('body').on('click', '.down-page', function(){
        $(this).toggleClass('open');
    });

    $('.nav-toggle').click(function(){
        if (headerOpen){
            closeHeader();
        } else {
            openHeader();
        }
    });

    $('#header a').click(function(){
        closeHeader();
    });

    new Image().src = "img/speechbubble.png";
    new Image().src = "img/speechbubble-large.png";
    new Image().src = "img/speechbubble-mediumlarge.png";
    new Image().src = "img/speechbubble-medium.png";
    new Image().src = "img/speechbubble-long.png";
    new Image().src = "img/close-menu.jpg";

});

function openHeader(){
    if(!headerOpen){
        $('#header').stop().animate({left: 160}, 500, 'easeInOutExpo');
        $('.nav-toggle').css('background','url("img/close-menu.jpg")')
        headerOpen = true;
    }
}

function closeHeader(){
    if(headerOpen){
        $('#header').stop().animate({left: 0}, 500, 'easeInOutExpo');
        $('.nav-toggle').css('background','url("img/open-menu.jpg")')
        headerOpen = false;
    }
}