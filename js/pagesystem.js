var scrollingEnabled = true;
var scrollingThreshold = 1;

var config = { 
	'baseURL'           : ($('base').attr('href')),
	'targetURLPosition' : (window.location.href.substr(0,window.location.href.lastIndexOf('/')+1)).split("/").length-1
}

if(window.location.href.indexOf('staging.citrusmedia.com')>-1){
	config.baseURL = 'http://staging.citrusmedia.com/signum/'
}

browser = checkIE();

// -- expansions to jquery -- //

$.fn.nextOrFirst = function(selector){
	var next = this.next(selector);
	return (next.length) ? next : this.prevAll(selector).last();
};

$.fn.prevOrLast = function(selector){
	var prev = this.prev(selector);
	return (prev.length) ? prev : this.nextAll(selector).last();
};

// -- Page pre-loading -- //

/*imagesLoaded( document.querySelector('img'), function( instance ) {
    $(".loading, .dark-loading").hide();
});*/

// -- objects -- //

function pageContainer( url, obj ) {
	this.url = url;
	this.obj = obj;

	if(pages[url] == null){
		pages[url] = this;
	}

	this.loadPage = function() {
		pages[this.obj.children(':first').attr('id')].loadPage();
	}
}

var redirects = [];

function pageRedirect ( url, page ) {
	this.url = url;
	this.page = page;
	redirects[url] = this;
}

function updatePage(url){
	var page = pages[url];
	if (page.obj.parents('.page-container').length) {
		elem = page.obj.parents('.page-container');
		page.container = new pageContainer( elem.attr('id'), elem );
	}else {
		page.container = false;
	}
}

function page( url, obj ) {
	this.url = url;
	this.obj = obj;

	this.loadFunctions = [];
	this.unloadFunctions = [];

	pages[url] = this;

	if (obj.parents('.page-container').length) {
		elem = obj.parents('.page-container');
		this.container = new pageContainer( elem.attr('id'), elem );
	}else {
		this.container = false;
	}

	var speed = 1000;
	var ease = 'easeInOutExpo';

	this.loadPage = function( fromTop ) {
		if (!(currentPage.url == this.url)){
			for (i=0; i<currentPage.unloadFunctions.length; i++){
				(currentPage.unloadFunctions[i])();
			}
			scrollingEnabled = false;
			if (fromTop){
				currentPage.obj.stop().css({top: 0}).animate({top: '100%'}, speed, ease, function(){
					$(this).css({top: 0, zIndex: 1});
					scrollingEnabled = true;
				});
				this.obj.stop().css({top: '-100%', zIndex: 2}).animate({top: 0}, speed, ease);
			} else {
				currentPage.obj.stop().css({top: 0}).animate({top: "-100%"}, speed, ease, function(){
					$(this).css({top: 0, zIndex: 1});
					scrollingEnabled = true;
				});
				this.obj.stop().css({top: '100%', zIndex: 2}).animate({top: 0}, speed, ease);
			}
			currentPage = this;
			if (this.url=="home"){
				if(browser >= 10 || browser == -1){
					window.history.pushState(this.url, this.url, (config.baseURL));
				} else {
					window.location = ('#');
				}
			}else{
				if(browser >= 10 || browser == -1){
					window.history.pushState(this.url, this.url, (config.baseURL)+this.url);
				} else {
					window.location = ('#/'+this.url);
				}
			}
			for (i=0; i<this.loadFunctions.length; i++){
				(this.loadFunctions[i])();
			}
		}
	}
}


// --- On page load, render page --- //

var pages = {};

$('body').on('click', 'a:not(".fancybox")', function( event ){
	event.preventDefault();
	if($(this).attr('href').indexOf("page:") > -1){
		url = $(this).attr('href').split(":")[1].split("->");
		if (pages[url[0]]){
			if (url[1]=="next"){
				cp = currentPage.obj;
				select = cp.nextOrFirst('.page').attr('id');
				pages[select].loadPage();
			}else if (url[1]=="prev"){
				cp = currentPage.obj;
				select = cp.prevOrLast('.page').attr('id');
				pages[select].loadPage(true);
			}else {
				pages[url].loadPage($(this).data('direction') == 'top');
			}
		}else{
			if (redirects[url]){
				redirects[url].page.loadPage();
			} else {
				console.warn(($(this).attr('href')) + ' - Page can not be found or did not load properly');
				pages['error'].loadPage();
			}
		}
	}else{
		if($(this).attr('href').indexOf("#") == -1){
			window.location = $(this).attr('href');
		}
	}
});

$('.page').each(function(){
	new page( $(this).attr('id'), $(this) );
});

// console.log(pages);

currentPage = pages[firstPage];
use(firstPage).css('z-index', 2);

$('.page-redirect').each(function(){
	new pageRedirect ( $(this).attr('data-from'), pages[ $(this).attr('data-to') ] );
});

if(browser >= 10 || browser == -1){
	if (window.location.href.split("/")[(config.targetURLPosition)]) {
		if (window.location.href.indexOf('#')>(-1)){ // If the url uses the # system, convert to regular system
			urlTarget = window.location.href.split("#/")[1];
			config.baseURL = config.baseURL.split('#')[0];
		} else {
			urlTarget = window.location.href.split((config.baseURL))[1];
		}
		if (urlTarget in pages) {
			pages[urlTarget].loadPage();
		}else if (urlTarget in redirects){
			redirects[urlTarget].page.loadPage();
		}else {
			pages['error'].loadPage();
		}
	}
} else {
	if (window.location.href.indexOf('#')==(-1) && (window.location.href!=(config.baseURL)) ) { // If URL is in IE10+ format
		window.location = (config.baseURL)+"#/" + window.location.href.split((config.baseURL))[1];
	} else {
		if (window.location.href == (config.baseURL)) { // if trying to get to home page
			pages['home'].loadPage();
		} else {
			if (window.location.href.split("#/")[1] != "") {
				urlTarget = window.location.href.split("#/")[1];
				if (urlTarget in pages) {
					pages[urlTarget].loadPage();
				}else {
					pages['error'].loadPage();
				}
			}
		}
	}
}

function checkIE() {
	var rv = -1;
	if (navigator.appName == 'Microsoft Internet Explorer')
	{
		var ua = navigator.userAgent;
		var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null) {
			rv = parseFloat( RegExp.$1 );
		}
	}
	return rv;
}

var subpages = {
	'about/one': {prev: null, next: 'about/two'},
	'about/two': {prev: 'about/one', next: null},

	'who-we-are/one': {prev: null, next: 'who-we-are/two'},
	'who-we-are/two': {prev: 'who-we-are/one', next: null},

	'projects/americredit': {prev: null, next: 'projects/bell'},
	'projects/bell': {prev: 'projects/americredit', next: 'projects/scotia-bank'},
	'projects/scotia-bank': {prev: 'projects/bell', next: 'projects/kawartha-glen'},
	'projects/kawartha-glen': {prev: 'projects/scotia-bank', next: 'projects/420-george'},
	'projects/420-george': {prev: 'projects/kawartha-glen', next: 'projects/ems'},
	'projects/ems': {prev: 'projects/420-george', next: 'projects/unicity'},
	'projects/unicity': {prev: 'projects/ems', next: null},
};

$(window).mousewheel(function(e){
	console.log(e.deltaY > 0 ? 'up' : 'down', e.deltaY)
	if(scrollingEnabled){
		if(e.deltaY >= scrollingThreshold){
			//up
			if(subpages[currentPage.url] != undefined && subpages[currentPage.url].prev){
				pages[subpages[currentPage.url].prev].loadPage(true);
			}
		} else if(e.deltaY <= -(scrollingThreshold)){
			//down
			if(subpages[currentPage.url] != undefined && subpages[currentPage.url].next){
				pages[subpages[currentPage.url].next].loadPage();
			}
		}
	}
});

/*var skip = false;
$(window).mousewheel(function(event) {
    if (currentPage.container){
    	if (currentPage.container.obj.children('.page').length == 2){
	        if(event.deltaY < -15 && skip==false){
	            console.log(event.deltaY)
	            select = currentPage.obj.next('.page').attr('id');
	            pages[select].loadPage(false);
	            skip=true
	            setTimeout(function(){
	                skip=false;
	            },2000)
	        }else if(event.deltaY > 15 && skip==false){
	            console.log(event.deltaY)
	            select = currentPage.obj.prev('.page').attr('id');
	            pages[select].loadPage(true);
	            skip=true
	            setTimeout(function(){
	                skip=false;
	            },2000)
	        }
		} else {
	        if(event.deltaY < -15 && skip==false){
	            console.log(event.deltaY)
	            select = currentPage.obj.nextOrFirst('.page').attr('id');
	            pages[select].loadPage(false);
	            skip=true
	            setTimeout(function(){
	                skip=false;
	            },2000)
	        }else if(event.deltaY > 15 && skip==false){
	            console.log(event.deltaY)
	            select = currentPage.obj.prevOrLast('.page').attr('id');
	            pages[select].loadPage(true);
	            skip=true
	            setTimeout(function(){
	                skip=false;
	            },2000)
	        }
	    }
    }
});*/

// --- Helper functions for pages --- //

function use( url ) {
	if (url in pages) {
		return pages[url].obj;
	}else {
		console.error('USE() - Can\'t find page:'+url);
		return false;
	}
}

function currentPage(){
	return pages[currentPage].url;
}

function load( url ) {
	if (url in pages) {
		return pages[url].loadPage();
	}else {
		console.error('LOAD() - Can\'t find page:'+url);
		return false;
	}
}

function onLoad( url, func ) {
	if (url in pages) {
		pages[url].loadFunctions.push(func);
	}else {
		console.error('ONLOAD() - Can\'t find page:'+url);
		return false;
	}
}

function onUnload( url, func ) {
	if (url in pages) {
		pages[url].unloadFunctions.push(func);
	}else {
		console.error('ONUNLOAD() - Can\'t find page:'+url);
		return false;
	}
}

var pagesLoaded = new Array();
$(window).load(function(){
	scale();
	$(window).resize(function(){
		scale();
	});
	$('.fancybox').fancybox();
	loadNextPage();
});


function loadNextPage(){
	if(pagesLoaded.length < pageList.length){
		for(var i = 0; i < pageList.length; i++){
			if(pagesLoaded.indexOf(pageList[i]) == -1){
				var nextPage = pageList[i];
				$.ajax({
					url: './',
					data: {uri: nextPage, partial: 1},
					type: 'GET',
					contentType: "application/json; charset=utf-8",
					dataType: 'json',
					success: function(data){
						var newPage = $(data.content);
						new page( newPage.attr('id'), newPage );
						if(data.containerId){
							var container = $('#'+data.containerId);
							if(container.length == 0){
								container = $(data.container);
								$('#pages').append(container);
							}
							container.append(newPage);
						} else {
							$('#pages').append(newPage);
						}
						pagesLoaded.push(nextPage);
						updatePage(nextPage);
						// console.log(nextPage+' loaded');
						loadNextPage();
					}
				});
				break;
			}
		}
	}
	else
	{
		// console.log(pages);
		// console.log('pages loaded');
		scale();
		$('.fancybox').fancybox();
	}
}

function scale() {
	coef = $(window).height()/5;
	$('.imgbox').each(function(){
		posy = parseInt($(this).attr('data-pos-y'));
		posx = parseInt($(this).attr('data-pos-x'));
		$(this).css({
			bottom: (posy*coef)+"px",
			right: (posx*coef)+"px"
		})
	})
	$('.gallery .h1').css('height',coef+"px");
	$('.gallery .h2').css('height',(coef*2)+"px");
	$('.gallery .w1').css('width',coef+"px");
	$('.gallery .w2').css('width',(coef*2)+"px");
	$('.gallery').css({
		width: (coef*3)+"px",
		right: -coef+"px",
		bottom: -coef+"px"
	});
}