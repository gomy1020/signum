browser = checkIE();

if ((browser >= 10 || browser == -1)) { // If browser is IE10+ or another browser

	// --- Page Object ---

	function page( url, obj ) {
		this.url = url;
		this.obj = obj;

		pages[url] = this;

		this.loadPage = function() {
			if (!(currentPage.url == this.url)){
				currentPage.obj.animate({
					top: "-100%"
				},1000,function(){
					$(this).css('top','100%');
				});
				this.obj.animate({
					top: "0"
				},1000);
				currentPage = this;
				if (this.url=="home"){
					window.history.pushState(this.url, this.url, '/');
				}else{
					window.history.pushState(this.url, this.url, '/'+this.url);
				}
			}
		}
	}

	// ------------------

	var pages = {};

	$('a').click(function( event ){
		event.preventDefault();
		if($(this).attr('href').indexOf("page:") > -1){
			url = $(this).attr('href').split(":")[1];
			if (pages[url]){
				pages[url].loadPage();
			}else{
				pages['error'].loadPage();
			}
		}else{
			window.location = $(this).attr('href');
		}
	});

	$('.page').each(function(){
		new page( $(this).attr('id'), $(this) );
	})

	currentPage = pages['home'];

	if (window.location.href.split("/")[3]) {
		if (window.location.href.indexOf('#')>(-1)){ // If the url uses the # system, convert to regular system
			urlTarget = window.location.href.split("#/")[1];
		}	else {
			urlTarget = window.location.href.split("/")[3];
		}
		pages[urlTarget].loadPage();
	}

} else { // If IE9 and under (This system uses #s in the URLs, it is very compatible, but can be glitchy due to the nature of id links)

	// --- Page Object ---

	function page( url, obj ) {
		this.url = url;
		this.obj = obj;

		pages[url] = this;

		this.loadPage = function() {
			if (!(currentPage.url == this.url)){
				currentPage.obj.animate({
					top: "-100%"
				},1000,function(){
					$(this).css('top','100%');
				});
				this.obj.animate({
					top: "0"
				},1000);
				currentPage = this;
				if (this.url=="home"){
					window.location = ('/');
				}else{
					window.location = ('/#/'+this.url);
				}
			}
		}
	}

	// ------------------

	var pages = {};

	$('a').click(function( event ){
		event.preventDefault();
		if($(this).attr('href').indexOf("page:") > -1){
			url = $(this).attr('href').split(":")[1];
			if (pages[url]){
				pages[url].loadPage();
			}else{
				pages['error'].loadPage();
			}
		}else{
			window.location = $(this).attr('href');
		}
	});

	$('.page').each(function(){
		new page( $(this).attr('id'), $(this) );
	})

	currentPage = pages['home'];

	if (window.location.href.split("#/")[1] != "") {
		urlTarget = window.location.href.split("#/")[1];
		pages[urlTarget].loadPage();
	}

}

function checkIE() {
	var rv = -1;
	if (navigator.appName == 'Microsoft Internet Explorer')
	{
		var ua = navigator.userAgent;
		var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null) {
			rv = parseFloat( RegExp.$1 );
		}
	}
	return rv;
}