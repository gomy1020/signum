function setupMobileGallery( page ) {
	$('.'+page+' .gallery-mobile .clip .imgbox-mobile').css('left','-390px');
	$('.'+page+' .gallery-mobile .clip .imgbox-mobile:first-child').css('left','0px').addClass('current');
	$('.'+page+' .go-left-mobile').click(function(){
		$('.gallery-mobile .clip .imgbox-mobile.current').animate({
			left: "345px"
		},100,function(){
			$(this).css('left','-390px');
		});
		$('.'+page+' .gallery-mobile .clip .imgbox-mobile.current').removeClass('current').nextOrFirst('.imgbox-mobile').addClass('current');
		$('.'+page+' .gallery-mobile .clip .imgbox-mobile.current').animate({
			left: "0px"
		},100);
	});
	$('.'+page+' .go-right-mobile').click(function(){
		$('.'+page+' .gallery-mobile .clip .imgbox-mobile.current').animate({
			left: "-390px"
		},100);
		$('.'+page+' .gallery-mobile .clip .imgbox-mobile.current').removeClass('current').prevOrLast('.imgbox-mobile').addClass('current');
		$('.'+page+' .gallery-mobile .clip .imgbox-mobile.current').css('left','345px').animate({
			left: "0px"
		},100);
	});
};