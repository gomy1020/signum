var app = angular.module('signum', []);

function goTo( page ) {
	pageObj = $('.page.'+page);
	oldPageObj = $('.page.page-current');
	pageObj.animate({
		top: 0
	},1000,function(){
		oldPageObj.css({ top: '100%' });
		oldPageObj.removeClass('page-current');
		pageObj.addClass('page-current');
	});
	$location.url('/'+page);
}

$(document).ready(function(){
	$('nav a').click(function( event ){
		event.preventDefault();
		page = $(this).attr('href');
		goTo( page );
	})
})