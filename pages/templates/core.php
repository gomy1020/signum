<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <base href="{{ base_url }}">
        <link rel="shortcut icon" href="{{ base_url }}/favicon.ico" />
        <title>Signum</title>
        <?php /* ?>
        <script type="text/javascript">
            paceOptions = {
                restartOnRequestAfter: false,
                ajax: {
                    ignoreURLs: ['page:']
                }
            }
        </script>
        <link rel="stylesheet" type="text/css" href="css/pace-bar.css">
        <script type="text/javascript" src="js/pace.min.js"></script>
        <?php */ ?>
        {{ meta }}
        <!--style>
            .loading {
                z-index: 2147483647;
                position: absolute;
                top: 50%;
                left: 50%;
                margin-left: -213px;
                margin-top: -43px;
                color: #FFF;
                font-size: 18px;
                text-align: center;
            }
            .dark-loading {
                height: 100%;
                width: 100%;
                position: absolute;
                top: 0;
                left: 0;
                z-index: 2147483646;
                background: rgba(0,0,0,0.8);
            }
        </style-->
        <link rel="stylesheet" href="css/fonts/gudea/stylesheet.css">
        <link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/pages.css">
        <link rel="stylesheet" href="js/fancybox/jquery.fancybox.css">
        <?php /* ?>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <?php */ ?>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-61744269-2']);

            (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
        <script src="js/vendor/jquery-1.10.2.min.js"></script>
        <script src="js/jquery.backstretch.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
    </head>
    <body>
        <!--div class="dark-loading"></div>
        <div class="loading">
            <img src="img/loading-circle.svg" style="margin-bottom:30px"/><br/>
            Give us a second while we build your website experiance
        </div-->

        {{ nav }}
        
        <div id="pages">
        {{ content }}
        </div>

        <script>
        var firstPage = '<?php echo $uri ?>';
        var pageList = <?php echo json_encode($pageList) ?>;
        </script>
        {{ scripts }}

        {{ tracking }}

    </body>
</html>
