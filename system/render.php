<?php
if(isset($_GET['partial']))
{
	$templateRaw = '{{ content }}';
}
else
{
	ob_start();
	include($template);
	$templateRaw = ob_get_contents();
	@ob_end_clean();
}

$templateRaw = str_replace('{{ content }}', $pageContent, $templateRaw);
$templateRaw = str_replace('{{ base_url }}', $base_url, $templateRaw);

foreach($includesRaw as $ref => $data){
	$templateRaw = str_replace('{{ ' . $ref . ' }}', $data, $templateRaw);
}
foreach($routes as $route => $page)
{
	if (is_array($page)){
		$templateRaw = str_replace('{{ ' . $route . '->count }}', (count($page)), $templateRaw);
	}
}

if(isset($_GET['partial']))
{
	header('Content-Type: application/json');
	exit(json_encode(array(
		'containerId' => $containerId ? $containerId : false,
		'container' => $containerId ? $outputContainer : false,
		'content' => minify_output($templateRaw),
	)));
}
else
{
	echo $templateRaw;
}

function minify_output($buffer)
{
	$search = array(
		'/\>[^\S ]+/s',
		'/[^\S ]+\</s',
		'/(\s)+/s',
		'/\>\s\</s'
	);
	$replace = array(
		'>',
		'<',
		'\\1',
		'><'
	);
	$buffer = preg_replace($search, $replace, $buffer);
	return $buffer;
}