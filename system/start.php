<?php
$base_dir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
$base_url = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
$base_url .= '://'. $_SERVER['HTTP_HOST'];
$base_url .= $base_dir;

if(isset($_GET['uri']))
{
	$uri = $_GET['uri'];
}
else
{
	$uri = trim(str_replace($base_dir, '', $_SERVER['REQUEST_URI']), '/');
	if(empty($uri)) $uri = 'home';
}

$segments = explode('/', $uri);

$includesRaw = array();
if (count($includes)>0){
	foreach($includes as $ref => $include)
	{
		ob_start();
		include('pages/'.$include);
		$includeRawLoop = ob_get_contents();
		@ob_end_clean();

		if (strpos($includeRawLoop,'{{ repeat->') > -1) {
			foreach($routes as $route => $page)
			{
				if (is_array($page)){
					$loopwherestart = strpos($includeRawLoop,'{{ repeat->' . $route . ' }}');
					if($loopwherestart > -1){
						$loopwherestart = $loopwherestart + strlen('{{ repeat->' . $route . ' }}');
						$loopwhereend = strpos($includeRawLoop,'{{ /repeat->' . $route . ' }}');
						$looplength = $loopwhereend - $loopwherestart;
						$loopcontent = substr($includeRawLoop, $loopwherestart, $looplength);
						$i=1;
						$finalinclude = "";
						$result = "";
						foreach($page as $subroute => $subpage)
						{
							$result = $loopcontent;
							$result = str_replace('{{ page->route }}',($subroute),$result);
							$result = str_replace('{{ page->position }}',($i),$result);
							$result = str_replace('{{ page->end? }}',(count($page)==$i ? 'true' : 'false'),$result);
							$result = str_replace('{{ page->start? }}',($i==1 ? 'true' : 'false'),$result);
							$finalinclude .= $result;
							$i=$i+1;
						}
						$includeRawLoop = substr_replace($includeRawLoop,$finalinclude,($loopwherestart-strlen('{{ repeat->' . $route . ' }}')),($looplength+(strlen('{{ /repeat->' . $route . ' }}')*2)-1));
					}
				}
			}
		}
		$includesRaw[$ref] = $includeRawLoop;
	}
}
$route = $segments[0];
$subroute = isset($segments[1]) ? $segments[1] : null;
$page = $routes[$route];
$subpage = $subroute ? $page[$subroute] : null;
$pageList = array();

$outputContainer = '';
$containerId = '';

$pageContent = '';

if (is_array($page)){
	$keys = array_keys($page);
	$key_indexes = array_flip($keys);

	$position = $key_indexes[$subroute] + 1;

	$next_subroute = 'page:'.$route.'/'.($key_indexes[$subroute]+1 < count($keys) ? $keys[$key_indexes[$subroute]+1] : $keys[0]);
	$prev_subroute = 'page:'.$route.'/'.($key_indexes[$subroute]-1 >= 0 ? $keys[$key_indexes[$subroute]-1] : $keys[count($keys)-1]).'" data-direction="top';

	$outputContainerStart = '<span class="page-container ' . str_replace('/','-',$route) . '" id="' . $route . '">';

	if(!isset($_GET['partial'])) $pageContent .= $outputContainerStart;

	$containerId = $route;
	// $i=1;

	ob_start();
	echo '<div class="page ' . str_replace('/','-',$route) . '-' . str_replace('/','-',$subroute) . '" id="' . $route . '/' . $subroute . '">';
	include('pages/'.$subpage);
	echo '</div>';
	$result = ob_get_contents();
	$result = str_replace('{{ ' . $route . '->position }}', $position, $result);
	$result = str_replace('{{ ' . $route . '->next }}', $next_subroute, $result);
	$result = str_replace('{{ ' . $route . '->prev }}', $prev_subroute, $result);
	$result = str_replace('{{ ' . $route . '->end? }}', ($position == count($keys) ? 'true' : 'false'), $result);
	$result = str_replace('{{ ' . $route . '->start? }}', ($position==1 ? 'true' : 'false'), $result);
	$pageContent .= $result;
	@ob_end_clean();
	// $i=$i+1;
	
	$outputContainerEnd = "</span>";

	if(!isset($_GET['partial'])) $pageContent .= $outputContainerEnd;

	$outputContainer = $outputContainerStart.$outputContainerEnd;
} else {
	ob_start();
	echo '<div class="page ' . str_replace('/','-',$route) . '" id="' . $route . '">';
	include('pages/'.$page);
	echo '</div>';
	$pageContent .= ob_get_contents();
	@ob_end_clean();
}

if(!isset($_GET['partial']))
{
	foreach($routes as $route => $page)
	{
		if(is_array($page))
		{
			foreach($page as $subroute => $subpage)
			{
				if($uri != $route.'/'.$subroute) $pageList[] = $route.'/'.$subroute;
			}
		}
		else
		{
			if($uri != $route) $pageList[] = $route;
		}
	}
}

/*$pageContent = '';
foreach($routes as $route => $page)
{
	if (is_array($page)){
		$pageContent .= '<span class="page-container ' . str_replace('/','-',$route) . '" id="' . $route . '">';
		$i=1;
		foreach($page as $subroute => $subpage)
		{
			ob_start();
			echo '<div class="page ' . str_replace('/','-',$route) . '-' . str_replace('/','-',$subroute) . '" id="' . $route . '/' . $subroute . '">';
			include('pages/'.$subpage);
			echo '</div>';
			$result = ob_get_contents();
			$result = str_replace('{{ ' . $route . '->position }}',($i),$result);
			$result = str_replace('{{ ' . $route . '->end? }}',(count($page)==$i ? 'true' : 'false'),$result);
			$result = str_replace('{{ ' . $route . '->start? }}',($i==1 ? 'true' : 'false'),$result);
			$pageContent .= $result;
			@ob_end_clean();
			$i=$i+1;
		}
		$pageContent .= "</span>";
	}else{
		ob_start();
		echo '<div class="page ' . str_replace('/','-',$route) . '" id="' . $route . '">';
		include('pages/'.$page);
		echo '</div>';
		$pageContent .= ob_get_contents();
		@ob_end_clean();
	}
}*/

foreach($redirects as $from => $to){
	$pageContent .= "<div class='page-redirect' data-from='" . $from . "' data-to='" . $to . "'></div>";
}

include('system/render.php');