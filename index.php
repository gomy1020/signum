<?php
/*
~ Use template file x to render all pages ~

	$template = "x";
	
*/
$template = 'pages/templates/core.php';

/*
~ Links URL x to html file y ~

	array(
		"x" => "y.html"
	);

~ Links URL group x to html files y1, y2... ~

	array(
		"x" => array(
			"fileOne" => "y1.html",
			"fileTwo" => "y2.html"
		);
	);
	
*/
$routes = array(
	'home' => 'home.html',
	'error' => 'error.html',
	'about' => array(
		'one' => 'about.html',
		'two' => 'about-2.html'
	),
	'contact' => 'contact.html',
	'who-we-are' => array(
		'one' => 'who-we-are-1.html',
		'two' => 'who-we-are-2.html'
	),
	'projects' => array(
		'americredit' => 'projects/americredit.html',
		'bell' => 'projects/bell.html',
		'scotia-bank' => 'projects/scotiabank.html',
		'kawartha-glen' => 'projects/kawarthaglen.html',
		'420-george' => 'projects/420george.html',
		'ems' => 'projects/ems.html',
		'unicity' => 'projects/unicity.html'
	)
);

/*
~ Declare an include named x to be used when y is called within a page or template ~

	array(
		"x" => "y.html"
	);
	
*/
$includes = array(
	'meta' => 'includes/meta.html',
	'tracking' => 'includes/tracking.html',
	'scripts' => 'includes/scripts.html',
	'nav' => 'includes/nav.html',
	'projectsNav' => 'includes/projects-nav.html'
);

/*
~ Redirect requests for page x to page y ~

	array(
		"x" => "y"
	);

*/
$redirects = array(
	// "test" => "about"
);

include('system/start.php');?>